# rossum-exercise
An exercise for rossum's hiring process

requirements:
- Python 3.8 (I recommend to use 3.8 instead of 3.9)
- Docker
- The Python dependencies can be found in requirements.txt

## How to run for dev

I suggest using a virtual environment.

```sh
pip install -r requirements.txt
./run_dev.sh
```

## How to run in docker
```sh
docker build --tag rossum-exercise .
./run_docker.sh
```