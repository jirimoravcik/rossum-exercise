import base64, project.config
from app import app as flask_app

valid_credentials = base64.b64encode(
    "{}:{}".format(project.config.USERNAME, project.config.PASSWORD).encode("utf-8")
).decode("utf-8")


def test_rossum_exercise_optimistic():
    with flask_app.test_client() as client:
        with flask_app.app_context():
            response = client.post(
                "/export/123456",
                headers={"Authorization": "Basic " + valid_credentials},
            )
            assert response.get_json() == {"success": True}
