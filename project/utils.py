def get_response(success, **kwargs):
    return {"success": success, **kwargs}

def create_app():
    app = Flask(__name__)

    return app