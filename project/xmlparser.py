import xml.etree.cElementTree as ET
import dateutil.parser


def sanitize_date(x):
    return dateutil.parser.parse(x).isoformat()


def find_and_append_subelement_with_text(
    parent, target_name, section, schema_id, sanitizer=lambda x: x.replace("\n", " ")
):
    datapoint_query = lambda schema_id: './/datapoint[@schema_id="{}"]'.format(
        schema_id
    )
    ET.SubElement(parent, target_name).text = sanitizer(
        section.find(datapoint_query(schema_id)).text
    )


def parse(xml):
    et_elem = ET.fromstring(xml)
    root = ET.Element("InvoiceRegisters")
    invoices = ET.SubElement(root, "Invoices")
    for annotation_raw in et_elem.iter("annotation"):
        annotation = ET.SubElement(invoices, "Payable")
        content = annotation_raw.find("content")
        for section in content:
            section_id = section.attrib["schema_id"]
            if section_id == "invoice_info_section":
                find_and_append_subelement_with_text(
                    annotation, "InvoiceNumber", section, "invoice_id"
                )
                find_and_append_subelement_with_text(
                    annotation, "InvoiceDate", section, "date_issue", sanitize_date
                )
                find_and_append_subelement_with_text(
                    annotation, "DueDate", section, "date_due", sanitize_date
                )
                find_and_append_subelement_with_text(
                    annotation, "Iban", section, "iban"
                )
            elif section_id == "amounts_section":
                find_and_append_subelement_with_text(
                    annotation, "Amount", section, "amount_total_tax"
                )
                find_and_append_subelement_with_text(
                    annotation, "Currency", section, "currency"
                )
            elif section_id == "vendor_section":
                find_and_append_subelement_with_text(
                    annotation, "Vendor", section, "sender_name"
                )
                find_and_append_subelement_with_text(
                    annotation, "VendorAddress", section, "sender_address"
                )
            elif section_id == "line_items_section":
                details = ET.SubElement(annotation, "Details")
                tuples = section.iter("tuple")
                for t in tuples:
                    detail = ET.SubElement(details, "Detail")
                    find_and_append_subelement_with_text(
                        detail, "Amount", t, "item_amount_total"
                    )
                    find_and_append_subelement_with_text(
                        detail, "Quantity", t, "item_quantity"
                    )
                    find_and_append_subelement_with_text(
                        detail, "Notes", t, "item_description"
                    )

    return ET.tostring(root, encoding="unicode")
