import requests, base64
from .utils import get_response


def encode_base64_str(s):
    s_bytes = s.encode("utf-8")
    return base64.b64encode(s_bytes)


def export_converted(annotation_id, xml):
    try:
        response = requests.post(
            config.POST_URL,
            data={
                "annotationId": annotation_id,
                "content": encode_base64_str(xml),
            },
            timeout=10,
        )
        response.raise_for_status()
        return get_response(True)
    except:
        return get_response(False)
