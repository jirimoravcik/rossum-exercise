import os

USERNAME = os.environ.get("USERNAME", default="myUser123")
PASSWORD = os.environ.get("PASSWORD", default="secretSecret")
POST_URL = os.environ.get("POST_URL", default="https://my-little-endpoint.ok/rossum")