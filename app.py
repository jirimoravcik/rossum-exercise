from flask import Flask, request
from project import config, apicaller, xmlparser, exportpost
from project.utils import get_response

app = Flask(__name__)


def process_export(annotation_id, username, password):
    if username != config.USERNAME or password != config.PASSWORD:
        return get_response(False)

    api_caller = apicaller.MockApiCaller()
    token = api_caller.login(config.USERNAME, config.PASSWORD)
    xml_call_result = api_caller.get_annotation(token, annotation_id)
    if not xml_call_result["success"]:
        return get_response(False)

    raw_xml = xml_call_result["xml"]
    parsed_xml = xmlparser.parse(raw_xml)

    return exportpost.export_converted(annotation_id, parsed_xml)


@app.route("/export/<int:annotation_id>", methods=["POST"])
def export(annotation_id):
    username, password = (
        request.authorization["username"],
        request.authorization["password"],
    )
    return process_export(annotation_id, username, password)
